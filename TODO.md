# TODO

* Add more events such as InventoryClose (With a reason to know if this closing is in favor of another inventory or a global closing)

* Allow inventories to receive items on certain slots (can be configurable)

* Better Translation integration with TranslationContext (and event ?)

* Add JSON file support