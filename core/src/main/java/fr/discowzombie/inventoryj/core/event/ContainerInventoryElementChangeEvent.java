package fr.discowzombie.inventoryj.core.event;

import fr.discowzombie.inventoryj.core.element.ContainerElement;
import fr.discowzombie.inventoryj.core.element.ContainerInventory;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public class ContainerInventoryElementChangeEvent<G, I, P> extends ContainerInventoryUpdateEvent<G, I, P> {

    private static final long serialVersionUID = -7771113263849743342L;
    private final ContainerInventory<G, I, P> source;
    private final ContainerElement oldValue, newValue;

    public ContainerInventoryElementChangeEvent(ContainerInventory<G, I, P> source, ContainerElement oldValue, ContainerElement newValue) {
        super(source, "element", oldValue, newValue);
        this.source = source;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public ContainerInventory<G, I, P> getSource() {
        return this.source;
    }

    @Override
    public ContainerElement getOldValue() {
        return this.oldValue;
    }

    @Override
    public ContainerElement getNewValue() {
        return this.newValue;
    }
}
