package fr.discowzombie.inventoryj.core.event;

import fr.discowzombie.inventoryj.core.element.ContainerItem;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public class ContainerItemUpdateEvent<G, I, P> extends ContainerElementPropertyChangeEvent {

    private static final long serialVersionUID = -5731937740052297179L;
    private final ContainerItem<G, I, P> source;

    public ContainerItemUpdateEvent(ContainerItem<G, I, P> source, String propertyName, Object oldValue, Object newValue) {
        super(source, propertyName, oldValue, newValue);
        this.source = source;
    }

    @Override
    public ContainerItem<G, I, P> getSource() {
        return this.source;
    }
}
