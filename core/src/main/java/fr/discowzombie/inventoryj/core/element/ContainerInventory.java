/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.core.element;

import fr.discowzombie.inventoryj.core.event.ContainerInventoryElementChangeEvent;
import fr.discowzombie.inventoryj.core.event.ContainerInventoryUpdateEvent;
import fr.discowzombie.inventoryj.core.event.ContainerItemUpdateEvent;

import java.beans.PropertyChangeSupport;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * <p>Represent an inventory</p>
 *
 * @param <G> The in-game inventory
 * @param <I> The in-game itemRepresentation
 * @param <P> The in-game player
 * @author DiscowZombie (Mathéo CIMBARO)
 * @version 1.0
 */
public abstract class ContainerInventory<G, I, P> extends ContainerElement {

    private final Map<Integer, ContainerElement> content = new HashMap<>();
    private final PropertyChangeSupport containerInventorySupport;
    private final PropertyChangeSupport itemEventPoll;
    private final List<ContainerInventoryRepresentation<G, I, P>> representations = new ArrayList<>();
    private Consumer<P> closeConsumer = null;
    private ContainerItem<G, I, P> itemRepresentation;
    private Function<P, String> nameFunction;
    private int size = 9;

    public ContainerInventory(ContainerItem<G, I, P> item, Function<P, String> nameFunction) {
        this.itemEventPoll = new PropertyChangeSupport(this);
        this.containerInventorySupport = new PropertyChangeSupport(this);
        this.itemRepresentation = Objects.requireNonNull(item);
        this.setName(nameFunction);
    }

    public ContainerInventory(ContainerItem<G, I, P> item, int size, Function<P, String> nameFunction) {
        this(item, nameFunction);
        setSize(size);
    }

    public ContainerInventory(ContainerItem<G, I, P> item, String name) {
        this(item, a -> name);
    }

    /**
     * Returns a map of all elements with the slot as a key
     *
     * @return A immutable map containing all elements
     */
    public Map<Integer, ContainerElement> getContent() {
        return Collections.unmodifiableMap(this.content);
    }

    /**
     * Returns the close consumer on this inventory
     *
     * @return The close consumer, can be {@literal null}
     */
    public Consumer<P> getCloseConsumer() {
        return closeConsumer;
    }

    /**
     * Set the close consumer on this inventory
     *
     * @param closeConsumer The close consumer, can be {@literal null}
     */
    public void setCloseConsumer(Consumer<P> closeConsumer) {
        this.closeConsumer = closeConsumer;
    }

    /**
     * Returns the itemRepresentation who represent this inventory
     *
     * @return The itemRepresentation who represent this inventory
     */
    public ContainerItem<G, I, P> getItemRepresentation() {
        return itemRepresentation;
    }

    /**
     * Set the itemRepresentation who represent this inventory
     *
     * @param itemRepresentation The itemRepresentation representing this inventory, events are allowed. {@code null} values are not allowed.
     */
    public void setItemRepresentation(ContainerItem<G, I, P> itemRepresentation) {
        ContainerItem<G, I, P> prevItem = this.itemRepresentation;
        this.itemRepresentation = Objects.requireNonNull(itemRepresentation);
        this.itemEventPoll.firePropertyChange(new ContainerInventoryUpdateEvent<>(this, "itemRepresentation", prevItem, this.itemRepresentation));
    }

    /**
     * Returns the name of this inventory
     *
     * @return The name of this inventory, never {@code null}
     */
    public Function<P, String> getName() {
        return nameFunction;
    }

    /**
     * Set the name of this inventory
     *
     * @param nameFunction The function to obtain the name of this inventory
     * @throws NullPointerException If the provided function is {@code null}
     */
    public void setName(Function<P, String> nameFunction) {
        this.nameFunction = Objects.requireNonNull(nameFunction);
    }

    /**
     * Set the name of this inventory
     *
     * @param name The name of this inventory
     * @throws NullPointerException If the provided function is {@code null}
     */
    public final void setName(String name) {
        Objects.requireNonNull(name);
        this.setName(p -> name);
    }

    /**
     * Returns the size of this inventory
     *
     * @return The inventory size
     */
    public int getSize() {
        return size;
    }

    /**
     * Set the size of this inventory
     *
     * @param size The inventory size
     * @throws IllegalArgumentException If the size is not a multiple of 9
     */
    public void setSize(int size) {
        if (size % 9 != 0) throw new IllegalArgumentException("Size % 9 != 0");
        this.size = size;
    }

    /**
     * Set an element on a specific slot
     *
     * @param slot    The slot
     * @param element The element to set, can be {@code null} if this slot should be empty
     * @throws IndexOutOfBoundsException If the slot is negative or outside of the inventory
     */
    public void setElement(int slot, ContainerElement element) {
        if (slot < 0 || slot >= this.getSize()) throw new IndexOutOfBoundsException("Slot index is out of bound");
        ContainerElement previousValue = this.content.put(slot, element);
        this.containerInventorySupport.firePropertyChange(new ContainerInventoryElementChangeEvent<>(this, previousValue, element));

        if (element == null) {
            // Want to clear the slot
            setElementListener(slot, p -> null);
        } else if (element instanceof ContainerItem) {
            // The item that was set
            ContainerItem<G, I, P> itemElement = ((ContainerItem<G, I, P>) element);
            // For all representation, rebuild this item and set-it
            setElementListener(slot, itemElement::constructItem);
            // Track changes on this item
            itemElement.getEventPoll().addPropertyChangeListener(listener -> {
                if (listener instanceof ContainerItemUpdateEvent) {
                    ContainerItem<G, I, P> itemSource = ((ContainerItemUpdateEvent<G, I, P>) listener).getSource();
                    setElementListener(slot, itemSource::constructItem);
                }
            });
        } else if (element instanceof ContainerInventory) {
            // The item (inventory) that was set
            ContainerInventory<G, I, P> inventoryElement = (ContainerInventory<G, I, P>) element;
            // For all representation, rebuild the item and set-it
            setElementListener(slot, p -> inventoryElement.getItemRepresentation().constructItem(p));
            // Track changes on the item representing this inventory
            inventoryElement.getItemRepresentation().getEventPoll().addPropertyChangeListener(listener -> {
                setElementListener(slot, p -> ((ContainerItem<G, I, P>) listener.getSource()).constructItem(p));
            });
            this.itemEventPoll.addPropertyChangeListener(listener -> {
                setElementListener(slot, p -> ((ContainerItem<G, I, P>) listener.getNewValue()).constructItem(p));
            });
        }
    }

    protected final void trackRepresentation(ContainerInventoryRepresentation<G, I, P> representation) {
        this.representations.add(representation);
    }

    public abstract void open(P player);

    private void setElementListener(int slot, Function<P, I> playerItemConsumer) {
        this.representations.forEach(representation ->
                representation.setElementListener(slot, playerItemConsumer.apply(representation.getPlayer())));
    }

    /**
     * Add an element on the next free slot
     *
     * @param element The element to add, can be {@code null} if this slot should be empty
     * @throws IndexOutOfBoundsException If this inventory do not have any free slot
     * @see #setElement(int, ContainerElement)
     */
    public final void addElement(ContainerElement element) throws IndexOutOfBoundsException {
        OptionalInt freeSlot = this.getFreeSlot();
        if (!freeSlot.isPresent()) throw new IndexOutOfBoundsException("No free slot on this inventory");
        this.setElement(freeSlot.getAsInt(), element);
    }

    /**
     * Returns the first free slot if this container if any
     *
     * @return The first free slot or {@link OptionalInt#empty()} if this container is full
     */
    public final OptionalInt getFreeSlot() {
        for (int i = 0; i < this.getSize(); ++i) {
            if (!this.content.containsKey(i))
                return OptionalInt.of(i);
        }
        return OptionalInt.empty();
    }

    public final void destroyRepresentation(ContainerInventoryRepresentation<G, I, P> representation) {
        this.representations.remove(representation);
    }
}
