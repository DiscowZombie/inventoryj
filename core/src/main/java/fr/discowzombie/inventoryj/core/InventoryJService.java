/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.core;

import fr.discowzombie.inventoryj.core.element.ContainerInventoryRepresentation;

import java.util.LinkedList;
import java.util.Optional;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public interface InventoryJService<G, I, P> {

    Optional<? extends ContainerInventoryRepresentation<G, I, P>> getCurrentInventory(P user);

    /**
     * Returns a copy of the history. Trying to add or remove elements from this list can do nothing or throw exception depending on the implementation
     *
     * @param user The user
     * @return The full history or an empty list if the user has not any opened inventory
     */
    LinkedList<? extends ContainerInventoryRepresentation<G, I, P>> getHistory(P user);

    default void clearHistory(P user) {
        clearHistory(user, true);
    }

    void clearHistory(P user, boolean closeInventory);

}
