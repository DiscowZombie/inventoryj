/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.core.element;

import java.util.Map;

/**
 * <p>Represent an in-game implementation of a {@link ContainerInventory}</p>
 *
 * @param <G> The in-game inventory
 * @param <I> The in-game item
 * @param <P> The in-game player
 * @author DiscowZombie (Mathéo CIMBARO)
 * @version 1.0
 */
public abstract class ContainerInventoryRepresentation<G, I, P> {

    private final ContainerInventory<G, I, P> inventory;
    private final P player;

    public ContainerInventoryRepresentation(ContainerInventory<G, I, P> inventory, P player) {
        this.inventory = inventory;
        this.player = player;
    }

    public P getPlayer() {
        return player;
    }

    /**
     * This method should put items on the inventory.
     *
     * <p>You should override-it and use-it to set the items on your inventory.</p>
     *
     * @param slot The slot where this item leave
     * @param item The item to set, {@code null} is allowed to clear the slot
     */
    protected abstract void setElementListener(int slot, I item);

    /**
     * Open the inventory for the provided player
     *
     * <p>You should override this method and open the inventory for the player.</p>
     *
     * @param player The player
     */
    public abstract void open(P player);

    /**
     * Method called when this representation is created
     *
     * <p>You should override this method and put all items passed as parameter on your inventory.</p>
     *
     * @param items The items to set
     */
    protected abstract void initRepresentation(Map<Integer, ContainerElement> items);

    public ContainerInventory<G, I, P> getInventory() {
        return inventory;
    }

    public final void destroy() {
        this.inventory.destroyRepresentation(this);
    }
}
