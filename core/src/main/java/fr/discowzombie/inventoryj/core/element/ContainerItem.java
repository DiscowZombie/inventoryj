/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.core.element;

import fr.discowzombie.inventoryj.core.InventoryClickCallback;
import fr.discowzombie.inventoryj.core.event.ContainerItemUpdateEvent;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * <p>Represent a item that can be on a container</p>
 *
 * @param <G> The in-game inventory
 * @param <I> The in-game item
 * @param <P> The in-game player
 * @author DiscowZombie (Mathéo CIMBARO)
 * @version 1.0
 */
public abstract class ContainerItem<G, I, P> extends ContainerElement {

    private final PropertyChangeSupport eventPoll;
    private I item;
    private Function<P, String> nameFunction = null;
    private Function<P, List<String>> descriptionFunction = null;
    private List<InventoryClickCallback> clickCallbacks = new ArrayList<>();

    public ContainerItem(I item) {
        this.eventPoll = new PropertyChangeSupport(this);
        this.item = Objects.requireNonNull(item);
    }

    /***
     * Returns the support attached to this item
     *
     * @return The attached support
     */
    PropertyChangeSupport getEventPoll() {
        return eventPoll;
    }

    /**
     * Returns the in-game item
     *
     * @return The in-game item
     */
    public I getItem() {
        return item;
    }

    /**
     * Set the in-game item
     *
     * @param item The in-game item
     */
    public void setItem(I item) {
        I prevItem = this.item;
        this.item = Objects.requireNonNull(item);
        this.eventPoll.firePropertyChange(new ContainerItemUpdateEvent<>(this, "item", prevItem, this.item));
    }

    /**
     * Returns the function to obtains the item name
     *
     * @return The function to have the name, or {@code null} to use the item display name
     */
    public Function<P, String> getName() {
        return nameFunction;
    }

    /**
     * Set the function to obtains the item name
     *
     * @param nameFunction The function or {@code null} to use the item display name
     */
    public void setName(Function<P, String> nameFunction) {
        Function<P, String> prevNameFunction = this.nameFunction;
        this.nameFunction = nameFunction;
        this.eventPoll.firePropertyChange(new ContainerItemUpdateEvent<>(this, "name", prevNameFunction, this.nameFunction));
    }

    /**
     * Set the function to obtains the item name
     *
     * @param name The name to set, {@code null} is not allowed
     */
    public void setName(String name) {
        setName(a -> Objects.requireNonNull(name));
    }

    /**
     * Returns the function to obtains the item lore
     *
     * @return The function to have the item lore, or {@code null} to use item lore
     */
    public Function<P, List<String>> getDescription() {
        return descriptionFunction;
    }

    /**
     * Set the function to obtains the item lore
     *
     * @param descriptionFunction The function or {@code null} to use the lore
     */
    public void setDescription(Function<P, List<String>> descriptionFunction) {
        Function<P, List<String>> prevDescriptionFunction = this.descriptionFunction;
        this.descriptionFunction = descriptionFunction;
        this.eventPoll.firePropertyChange(new ContainerItemUpdateEvent<>(this, "description", prevDescriptionFunction, this.descriptionFunction));
    }

    /**
     * Set the function to obtains the item lore
     *
     * @param description The function to set, {@code null} is not allowed
     */
    public void setDescription(List<String> description) {
        setDescription(a -> Objects.requireNonNull(description));
    }

    public abstract I constructItem(P player);

    /**
     * Get all events bind on this item
     *
     * @return A collection of all events
     */
    public List<InventoryClickCallback> getEvents() {
        return Collections.unmodifiableList(clickCallbacks);
    }

    /**
     * Clear all events bind on this item
     */
    public void clearEvents() {
        this.clickCallbacks.clear();
    }

    /**
     * Add a event on this item
     *
     * @param callback The event to add
     */
    public void addEvent(InventoryClickCallback<G, I, P> callback) {
        this.clickCallbacks.add(callback);
    }

    /**
     * Remove a event from this item
     *
     * @param callback The event to remove
     */
    public void removeEvent(InventoryClickCallback<G, I, P> callback) {
        this.clickCallbacks.remove(callback);
    }

}
