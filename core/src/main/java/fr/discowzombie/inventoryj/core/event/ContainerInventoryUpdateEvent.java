package fr.discowzombie.inventoryj.core.event;

import fr.discowzombie.inventoryj.core.element.ContainerInventory;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public class ContainerInventoryUpdateEvent<G, I, P> extends ContainerElementPropertyChangeEvent {

    private static final long serialVersionUID = -6289360565787510333L;
    private final ContainerInventory<G, I, P> source;

    public ContainerInventoryUpdateEvent(ContainerInventory<G, I, P> source, String propertyName, Object oldValue, Object newValue) {
        super(source, propertyName, oldValue, newValue);
        this.source = source;
    }

    @Override
    public ContainerInventory<G, I, P> getSource() {
        return this.source;
    }
}
