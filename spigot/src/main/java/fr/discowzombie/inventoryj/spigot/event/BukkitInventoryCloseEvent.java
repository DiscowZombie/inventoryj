package fr.discowzombie.inventoryj.spigot.event;

import fr.discowzombie.inventoryj.spigot.impl.BukkitInventoryRepresentation;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 * @version 1.0
 */
public class BukkitInventoryCloseEvent extends Event {

    private static final HandlerList HANDLER_LIST = new HandlerList();

    private final BukkitInventoryRepresentation bukkitInventory;
    private final CloseReason reason;

    public BukkitInventoryCloseEvent(final BukkitInventoryRepresentation bukkitInventory, final CloseReason reason) {
        this.bukkitInventory = bukkitInventory;
        this.reason = reason;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }

    public BukkitInventoryRepresentation getBukkitInventory() {
        return bukkitInventory;
    }

    public CloseReason getReason() {
        return reason;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public enum CloseReason {
        /**
         * The current inventory has been closed deliberately
         */
        CLOSE,
        /**
         * The user has open another inventory, that's why the current inventory has been closed
         */
        OPEN_INVENTORY
    }
}
