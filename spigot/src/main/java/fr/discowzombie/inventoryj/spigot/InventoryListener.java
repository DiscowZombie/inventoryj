/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.spigot;

import fr.discowzombie.inventoryj.core.context.ClickType;
import fr.discowzombie.inventoryj.core.context.InventoryAction;
import fr.discowzombie.inventoryj.core.context.InventoryClickContext;
import fr.discowzombie.inventoryj.core.element.ContainerElement;
import fr.discowzombie.inventoryj.core.element.ContainerInventory;
import fr.discowzombie.inventoryj.core.element.ContainerItem;
import fr.discowzombie.inventoryj.spigot.event.BukkitInventoryCloseEvent;
import fr.discowzombie.inventoryj.spigot.impl.BukkitInventoryJService;
import fr.discowzombie.inventoryj.spigot.impl.BukkitInventoryRepresentation;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public class InventoryListener implements Listener {

    private final static Set<UUID> persistentInventory = new HashSet<>();
    private final BukkitInventoryJService inventoryJService;

    public InventoryListener(BukkitInventoryJService inventoryJService) {
        this.inventoryJService = inventoryJService;
    }

    private static void destroy(BukkitInventoryRepresentation representation, Player player, BukkitInventoryCloseEvent closeEvent) {
        // Call the event
        Bukkit.getPluginManager().callEvent(closeEvent);

        final Consumer<Player> closeConsumer = representation.getInventory().getCloseConsumer();
        if (closeConsumer != null)
            closeConsumer.accept(player);

        // This representation can be destroyed
        representation.destroy();
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) return;
        Player player = (Player) event.getWhoClicked();

        // The player is only interacting with his own inventory
        if (event.getInventory() instanceof PlayerInventory && event.getClickedInventory() instanceof PlayerInventory)
            return;

        // The player is not on any GUI
        Optional<BukkitInventoryRepresentation> inventory = this.inventoryJService.getCurrentInventory(player);
        if (!inventory.isPresent())
            return;

        // The action seems to be a drag and drop action, disable-it
        if (!event.getInventory().equals(event.getClickedInventory())) {
            event.setCancelled(true);
            return;
        }

        // No item is present here, ignore
        if (event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR))
            return;

        ContainerElement element = inventory.get().getInventory().getContent().get(event.getSlot());

        // This slot seems to have an item, but the item is not properly register
        if (element == null) {
            event.setCancelled(true);
            throw new UnsupportedOperationException(String.format("Inventory %s seems to have an item not registered", event.getClickedInventory().getName()));
        }

        event.setCancelled(true);

        // Finally dispatch events
        if (element instanceof ContainerItem) {
            InventoryClickContext clickContext = new InventoryClickContext(InventoryAction.valueOf(event.getAction().toString()), ClickType.valueOf(event.getClick().toString()));
            ((ContainerItem<Inventory, ItemStack, Player>) element).getEvents()
                    .forEach(callback -> callback.onClick(event.getClickedInventory(), event.getCurrentItem(), player, clickContext));
        } else if (element instanceof ContainerInventory) {
            persistentInventory.add(player.getUniqueId());
            ((ContainerInventory) element).open(player);
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (!(event.getPlayer() instanceof Player)) return;
        Player player = (Player) event.getPlayer();

        final Optional<BukkitInventoryRepresentation> currentInventory = inventoryJService.getCurrentInventory(player);

        // Someone has called a function like "p.closeInventory()" without using an inventoryj-inventory
        if (!currentInventory.isPresent())
            return;

        // This inventory is closed in favor of another inventory (multiple-page or sub-inventory)
        final boolean remove = persistentInventory.remove(player.getUniqueId());

        // Create the event
        final BukkitInventoryCloseEvent closeEvent = new BukkitInventoryCloseEvent(currentInventory.get(),
                remove ? BukkitInventoryCloseEvent.CloseReason.OPEN_INVENTORY : BukkitInventoryCloseEvent.CloseReason.CLOSE);

        if (remove) {
            destroy(currentInventory.get(), player, closeEvent);
            return;
        }

        // Only dispatch this clear if it's a final closing
        this.inventoryJService.clearHistory(player, false);
        destroy(currentInventory.get(), player, closeEvent);
    }
}
