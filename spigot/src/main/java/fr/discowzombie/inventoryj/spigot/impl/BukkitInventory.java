/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.spigot.impl;

import fr.discowzombie.inventoryj.core.element.ContainerInventory;
import fr.discowzombie.inventoryj.core.element.ContainerItem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.function.Function;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public final class BukkitInventory extends ContainerInventory<Inventory, ItemStack, Player> {

    private static final Plugin PLUGIN = Bukkit.getPluginManager().getPlugin("InventoryJ");

    public BukkitInventory(ContainerItem<Inventory, ItemStack, Player> item, Function<Player, String> nameFunction) {
        super(item, nameFunction);
    }

    public BukkitInventory(ContainerItem<Inventory, ItemStack, Player> item, String name) {
        super(item, name);
    }

    public BukkitInventory(ContainerItem<Inventory, ItemStack, Player> item, int size, Function<Player, String> nameFunction) {
        super(item, size, nameFunction);
    }

    public static BukkitInventory of(ContainerItem<Inventory, ItemStack, Player> item, Function<Player, String> nameFunction) {
        return new BukkitInventory(item, nameFunction);
    }

    public static BukkitInventory of(ContainerItem<Inventory, ItemStack, Player> item, String name) {
        return new BukkitInventory(item, name);
    }

    public static BukkitInventory of(ContainerItem<Inventory, ItemStack, Player> item, int size, String name) {
        return of(item, size, a -> name);
    }

    public static BukkitInventory of(ContainerItem<Inventory, ItemStack, Player> item, int size, Function<Player, String> nameFunction) {
        return new BukkitInventory(item, size, nameFunction);
    }

    @Override
    public void open(Player player) {
        Bukkit.getScheduler().runTask(PLUGIN, () -> {
            BukkitInventoryRepresentation representation = new BukkitInventoryRepresentation(this, player);
            this.trackRepresentation(representation);
            representation.open(player);
        });
    }
}
