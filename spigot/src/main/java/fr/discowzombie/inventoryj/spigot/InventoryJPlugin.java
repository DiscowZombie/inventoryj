/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.spigot;

import fr.discowzombie.inventoryj.spigot.impl.BukkitInventoryJService;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public class InventoryJPlugin extends JavaPlugin {

    private static BukkitInventoryJService inventoryJService;

    public static BukkitInventoryJService getInventoryJService() {
        return inventoryJService;
    }

    @Override
    public void onEnable() {
        inventoryJService = new BukkitInventoryJService();

        Bukkit.getPluginManager().registerEvents(new InventoryListener(inventoryJService), this);
    }

}
