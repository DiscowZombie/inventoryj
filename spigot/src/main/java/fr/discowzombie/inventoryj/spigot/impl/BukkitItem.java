/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.spigot.impl;

import fr.discowzombie.inventoryj.core.element.ContainerItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public final class BukkitItem extends ContainerItem<Inventory, ItemStack, Player> {

    public static final BukkitItem DEFAULT_ITEM = BukkitItem.of(new ItemStack(Material.BARRIER));

    public BukkitItem(ItemStack itemStack) {
        super(itemStack);
    }

    public static BukkitItem of(ItemStack itemStack) {
        return new BukkitItem(itemStack);
    }

    @Override
    public ItemStack constructItem(Player player) {
        ItemStack bukkitItem = super.getItem();
        ItemMeta meta = bukkitItem.getItemMeta();
        if (super.getName() != null) {
            meta.setDisplayName(super.getName().apply(player));
        }
        if (super.getDescription() != null) {
            meta.setLore(super.getDescription().apply(player));
        }
        bukkitItem.setItemMeta(meta);
        return bukkitItem;
    }

}
