/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.spigot.impl;

import fr.discowzombie.inventoryj.core.element.ContainerElement;
import fr.discowzombie.inventoryj.core.element.ContainerInventory;
import fr.discowzombie.inventoryj.core.element.ContainerInventoryRepresentation;
import fr.discowzombie.inventoryj.core.element.ContainerItem;
import fr.discowzombie.inventoryj.spigot.InventoryJPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public final class BukkitInventoryRepresentation extends ContainerInventoryRepresentation<Inventory, ItemStack, Player> {

    private static final int MAX_TITLE_LENGTH = 32;

    private final Inventory inventory;

    public BukkitInventoryRepresentation(ContainerInventory<Inventory, ItemStack, Player> inventory, Player player) {
        super(inventory, player);
        this.inventory = Bukkit.createInventory(null, this.getInventory().getSize(), subString(this.getInventory().getName().apply(player), MAX_TITLE_LENGTH));
        this.initRepresentation(inventory.getContent());
    }

    private static String subString(String string, int maxLength) {
        return string.substring(0, Math.min(string.length(), maxLength));
    }

    @Override
    public void setElementListener(int slot, ItemStack item) {
        if (item == null) this.inventory.clear(slot);
        else this.inventory.setItem(slot, item);
    }

    @Override
    public void open(Player player) {
        if (!InventoryJPlugin.getInventoryJService().getCurrentInventory(player).isPresent()) {
            player.closeInventory();
        }
        player.openInventory(this.inventory);
        InventoryJPlugin.getInventoryJService().setCurrentInventory(player.getUniqueId(), this);
    }

    @Override
    public void initRepresentation(Map<Integer, ContainerElement> items) {
        items.forEach((slot, containedElement) -> {
            ItemStack it = null;
            if (containedElement instanceof ContainerItem) {
                it = (ItemStack) ((ContainerItem) containedElement).constructItem(this.getPlayer());
            } else if (containedElement instanceof ContainerInventory) {
                it = ((ContainerItem<Inventory, ItemStack, Player>) ((ContainerInventory) containedElement).getItemRepresentation()).constructItem(this.getPlayer());
            }
            this.inventory.setItem(slot, it);
        });
    }
}
