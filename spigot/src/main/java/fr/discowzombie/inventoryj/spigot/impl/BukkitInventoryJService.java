/*
 * MIT License
 *
 * Copyright (c) 2019 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.inventoryj.spigot.impl;

import fr.discowzombie.inventoryj.core.InventoryJService;
import fr.discowzombie.inventoryj.core.element.ContainerInventoryRepresentation;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 */
public final class BukkitInventoryJService implements InventoryJService<Inventory, ItemStack, Player> {

    private final Map<UUID, LinkedList<BukkitInventoryRepresentation>> history = new HashMap<>();

    @Override
    public Optional<BukkitInventoryRepresentation> getCurrentInventory(Player user) {
        return Optional.ofNullable(this.history.containsKey(user.getUniqueId()) ? this.history.get(user.getUniqueId()).getLast() : null);
    }

    void setCurrentInventory(UUID uuid, BukkitInventoryRepresentation representation) {
        LinkedList<BukkitInventoryRepresentation> list = this.history.getOrDefault(uuid, new LinkedList<>());
        list.addLast(representation);
        this.history.put(uuid, list);
    }

    @Override
    public LinkedList<? extends ContainerInventoryRepresentation<Inventory, ItemStack, Player>> getHistory(Player user) {
        return new LinkedList<>(this.history.getOrDefault(user.getUniqueId(), new LinkedList<>()));
    }

    @Override
    public void clearHistory(Player user, boolean closeInventory) {
        if (history.containsKey(user.getUniqueId())) {
            if (closeInventory) user.closeInventory();
            history.remove(user.getUniqueId());
        }
    }

}
