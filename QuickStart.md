## Quick Start

```java
        /*
        This object represent a model (i.e. rules) used to build an inventory.
        This object take a item as his representation. If this inventory is the root inventory, you can use BukkitItem.DEFAULT_ITEM which produce a inventory without specific representation
         */
        BukkitInventory inventory = new BukkitInventory(BukkitItem.DEFAULT_ITEM);

        // This function will be called for each player to obtain the name of this inventory
        inventory.setName(pl -> String.format("%s inventory", pl.getName()));

        /*
        Create a BukkitItem.
        BukkitItem is an object who contains a in-game item with actions to execute when clicking on it
         */
        BukkitItem item = new BukkitItem(new ItemStack(Material.DIAMOND));

        // Code executed when this diamond is clicked
        item.addEvent((inventory1, item1, player, clickContext) -> player.sendMessage(ChatColor.GOLD + "Hello"));

        /*
        Put the fresh created item on the inventory.
        This method fire a update on all childs (BukkitInventoryRepresentation)
         */
        inventory.setElement(0, item);

        /*
        Inventory can contains ContainerElement, i.e. BukkitItem or BukkitInventory
         */

        /*
        Finally open the fresh created inventory for this player.
        Each time this method is called, a new Inventory (from Bukkit) is created (a new instance of this BukkitInventory who act as model) is used.

        IF AND ONLY IF you want two players to have EXACTLY the same instance (i.e. the same Inventory (from Bukkit)) call the open method on the returned BukkitInventoryRepresentation)
        This is only useful if a player should show the inventory viewable by another.
        Keep in mind that updating a BukkitInventory will update ALL REPRESENTATIONS (all BukkitInventoryRepresentation)
         */
        inventory.open(Bukkit.getPlayer("Notch"));
```